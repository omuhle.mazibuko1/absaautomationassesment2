﻿Feature: ApiTests

All Api Tests for a dog Api found below

@test
Scenario Outline: List of all dog breeds
	Given I want to perform a request that will list all dogs 
	When I perform get request to produce a list of all dogs
	Then I should get a list of dogs and correct "<Status>" from api response
	And I should validate if Retreiver is on the list

	Examples: 
	|Status         |
	| OK            |

	@test
Scenario Outline: List of SubBreeds for Retriever
	Given I want to perform a request for the list of subBreeds for retriever
	When I perform get request  to list of subBreeds for "<Breed>"
	Then I should get correct  subBreeds "<SubBreed1>" and "<SubBreed2>" and "<SubBreed3>" and "<SubBreed4>" of Breed and correct "<Status>" from api response

	Examples: 
	| Status  | Breed     | SubBreed1           | SubBreed2      | SubBreed3           | SubBreed4       |
	| OK      | retriever |  chesapeake         |   curly        | flatcoated          |    golden       |


	@test
Scenario Outline: Produce a random image
	Given I want to perform a request to produce a random image
	When I perform get request to produce a random image
	Then I should see a random image and correct "<Status>" from api response

	Examples: 
	|Status         |
	| OK            |





