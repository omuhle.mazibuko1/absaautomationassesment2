﻿Feature: WebTests

All Web Selenium Tests found below

@test
Scenario Outline: Add First User to Web table
	Given I navigate to way to automation Website 
	When I land succesfully I should verify if Im on User List table
	Then I click Add user
	And I should enter First User details "<Fname>" and "<Lname>" and "<Username>" and "<Password>" and "<Email>" and "<Cell>"
	And I should ensure user is added on the list "<Fname>"

	Examples: 
	 | Fname       | Lname     | Username | Password      | Email               | Cell        |
	 |  FName1     |  Lname1   |  User    |  Pass1        | admin@mail.com      |   082555    |


	@test
Scenario Outline: Add Second User to Web table
	Given I navigate to way to automation Website 
	When I land succesfully I should verify if Im on User List table
	Then I click Add user
	And I should enter Second User details "<Fname>" and "<Lname>" and "<Username>" and "<Password>" and "<Email>" and "<Cell>"
	And I should ensure second user is added on the list "<Fname>"

	Examples: 
	| Fname       | Lname     | Username | Password      | Email                  | Cell        |
	|  FName2     |  Lname2   |  User    |  Pass2        | customer@mail.com      |   083444    |

