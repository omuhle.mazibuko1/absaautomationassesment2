﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsaCIBdigitalTechAssesment.PagesObjects
{
    public class WebObjects
    {
        public By getUserListTable()
        {
            By listTable = By.XPath("//td[text()='Tom']");
            return listTable;
        }

        public By getAddUserbutton()
        {
            By adduserbtn = By.XPath("//button[@type='add']");
            return adduserbtn;
        }

        public By getAddUserWindowTitle() 
        {
            By addUserWindiowTitle = By.XPath("//h3[@class='ng-binding']");
            return addUserWindiowTitle;
        }

        public By getFnameTextField()
        {
            By field = By.XPath("//input[@name='FirstName']");
            return field;
        }

        public By getLnameTextField()
        {
            By field2 = By.XPath("//input[@name='LastName']");
            return field2;
        }
        public By getUsernameTextField()
        {
            By field3 = By.XPath("//input[@name='UserName']");
            return field3;
        }

        public By getPasswordTextField()
        {
            By field4 = By.XPath("//input[@type='password']");
            return field4;
        }

        public By getCompanyAARadBtn()
        {
            By rad1 = By.XPath("//input[@value='15']");
            return rad1;
        }
        public By getCompanyBBRadBtn()
        {
            By rad2 = By.XPath("//input[@value='16']");
            return rad2;
        }

        public By getRoleDropDownList()
        {
            By roleDDL = By.XPath("//select[@name='RoleId']");
            return roleDDL;
        }
        public By getEmailTextField()
        {
            By email = By.XPath("//input[@type='email']");
            return email;
        }

        public By getCellPhoneField()
        {
            By cell = By.XPath("//input[@name='Mobilephone']");
            return cell;
        }

        public By getSaveButton() 
        {
            By save = By.XPath("//button[@class='btn btn-success']");
            return save;
        }
    }
}
