﻿using AbsaCIBdigitalTechAssesment.Reusable;
using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers;
using System;
using System.Net;
using System.Web;

namespace AbsaCIBdigitalTechAssesment.Functions.API
{
    public class APIFunctions
    {
        XmlConfigsReaderFunctions xmlreader = new XmlConfigsReaderFunctions();

        static RestClient client;
        static RestRequest request;
        static RestResponse Response;

        public void setupBaseUri()
        {
            
            client = new RestClient(xmlreader.GetApiBaseUrI());
        }
       
 
        public void doGetAllDogsRequestToProduceAllBreeds()
        {
            try
            {
                setupBaseUri();
                request = new RestRequest(xmlreader.GetAllDogsBreedsApiEndPoint());
                Response = client.Get(request);
                Console.WriteLine(Response.Content);
            }
            catch (HttpException ex)
            {
                ex.ToString();
            }
        }

        public void verifyAllDogsReturned()
        {
            if (Response.Content != string.Empty)
            {
                Console.WriteLine("All Breeds of dogs were returned....");
            }
            else
            {
                Assert.Fail("Breeds of Dogs were NOT returned....");
            }
        }

        public void verifyRetreiverBreedingFromResponse()
        {
            if (Response.Content != string.Empty)
            {
                if (Response.Content.Contains("retriever"))
                {
                    Console.WriteLine("Retriever is Within the List....");
                }
                else
                {
                    Assert.Fail("Retriever Breeder is NOT Within the List....");
                }
            }
           
        }

        public void verifyStatusDescriptionFromResponse(string expectedStatus)
        {
           
           if(Response.StatusDescription == expectedStatus)
           {
                    Console.WriteLine("The Response Status is a success....");
           }
            else
            {
                Assert.Fail("The Response Status is NOT a success........");
            }
        }

        public void doGetRetreiverSubBreedListRequest(string desiredbreed)
        {
          
            setupBaseUri();
            request = new RestRequest(xmlreader.GetRetrieverSubBreedsApiEndPoint());
            Response = client.Get(request);
        }

        public void verifyRetrieverSubBreeds(string breed1, string breed2, string breed3, string breed4)
        {
            try 
            {
                string jsonObject = @"{""message"":[""chesapeake"",""curly"",""flatcoated"",""golden""],""status"":""success""}";

                Dog dog = JsonConvert.DeserializeObject<Dog>(jsonObject);
                Console.WriteLine("The response is :" + dog.message);

                if (dog.message.Contains(breed1))
                {
                    Console.WriteLine("Sub Breeds Found...");
                }
                else 
                {
                    Console.WriteLine("Sub Breeds NOT Found...");
                }
            }
            catch (Exception ex) 
            {
                ex.ToString();
            }

        }

        public void doGetRandomImageRequest()
        {
            setupBaseUri(); 
            request = new RestRequest(xmlreader.GetRandomImageApiEndPoint());
            Response = client.Get(request);
        }

        public void verifyRandomImageFromResponse()
        {
            if (Response.Content.Contains(".jpg"))
            {
                Console.WriteLine("Random image returned....");
            }
            else
            {
                Assert.Fail("Random image Never returned....");
            }
        }
    }

    public class Dog
    {
        public string message { get; set; }
        
    }

}
