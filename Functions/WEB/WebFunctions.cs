﻿using AbsaCIBdigitalTechAssesment.PagesObjects;
using AbsaCIBdigitalTechAssesment.Reusable;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbsaCIBdigitalTechAssesment.Functions.WEB
{
    public class WebFunctions : WebObjects
    {
        public void NavigateToWaytoAutomate()
        {
            WebCoreFunctions.startWebDriver();
        }
        public bool isUserListTableSeen()
        {
            WebCoreFunctions.ImplicitlyWait(1000);
            bool userListTable = WebCoreFunctions.getElement(getUserListTable()).Displayed;

            if (userListTable != true)
            {
                Assert.Fail("User was NOT navigated to User List Table");
            }
            else 
            {
                Console.WriteLine("User was navigated Succesfully to the User List Table....");
            }

            return userListTable;
        }

        public void clickAddUser()
        {
            if (WebCoreFunctions.getElement(getAddUserbutton()).Enabled)
            {
                WebCoreFunctions.getElement(getAddUserbutton()).Click();
            }
            else
            {
                Assert.Fail("Add New User Button NOT Found...");
            }
        }

        public void addFirstUserDetails(string fname,string lname,string username,string password,string email,string cell) 
        {
            string userNameWithUniqueValue = username + WebCoreFunctions.GenerateRandomNumbers().ToString();

            WebCoreFunctions.switchFocusToActiveElement();
            WebCoreFunctions.ImplicitlyWait(1000);
            WebCoreFunctions.verifyTextOfElement(getAddUserWindowTitle());

            WebCoreFunctions.getElement(getFnameTextField()).SendKeys(fname);
            WebCoreFunctions.getElement(getLnameTextField()).SendKeys(lname);
            WebCoreFunctions.getElement(getUsernameTextField()).SendKeys(userNameWithUniqueValue);
            WebCoreFunctions.getElement(getPasswordTextField()).SendKeys(password);
            WebCoreFunctions.getElement(getCompanyAARadBtn()).Click();
            WebCoreFunctions.selectDropDownsByIndex(getRoleDropDownList(),3);
            WebCoreFunctions.getElement(getEmailTextField()).SendKeys(email);
            WebCoreFunctions.getElement(getCellPhoneField()).SendKeys(cell);
        }

        public void addSecondUserDetails(string fname, string lname, string username, string password, string email, string cell)
        {
            string userNameWithUniqueValue = username + WebCoreFunctions.GenerateRandomNumbers().ToString();

            WebCoreFunctions.switchFocusToActiveElement();
            WebCoreFunctions.ImplicitlyWait(1000);
            WebCoreFunctions.verifyTextOfElement(getAddUserWindowTitle());

            WebCoreFunctions.getElement(getFnameTextField()).SendKeys(fname);
            WebCoreFunctions.getElement(getLnameTextField()).SendKeys(lname);
            WebCoreFunctions.getElement(getUsernameTextField()).SendKeys(userNameWithUniqueValue);
            WebCoreFunctions.getElement(getPasswordTextField()).SendKeys(password);
            WebCoreFunctions.getElement(getCompanyBBRadBtn()).Click();
            WebCoreFunctions.selectDropDownsByIndex(getRoleDropDownList(), 2);
            WebCoreFunctions.getElement(getEmailTextField()).SendKeys(email);
            WebCoreFunctions.getElement(getCellPhoneField()).SendKeys(cell);
        }

        public void verifyFirstUserWasAddedSuccesful()
        {
            bool userAdded = WebCoreFunctions._driver.FindElement(By.XPath("//td[text()='FName1']")).Displayed;
            if (userAdded == true)
            {
                Console.WriteLine("User Added Succesfully....");
            }
            else
            {
                Assert.Fail("User was NOT Added Succesfully....");
            }
        }


        public void verifySecondUserWasAddedSuccesful()
        {
            bool userAdded = WebCoreFunctions._driver.FindElement(By.XPath("//td[text()='FName2']")).Displayed;
            if (userAdded == true)
            {
                Console.WriteLine("User Added Succesfully....");
            }
            else
            {
                Assert.Fail("User was NOT Added Succesfully....");
            }
        }

        public void clickSavebutton()
        { 
            WebCoreFunctions.getElement(getSaveButton()).Click();
        }


    }

    
}
