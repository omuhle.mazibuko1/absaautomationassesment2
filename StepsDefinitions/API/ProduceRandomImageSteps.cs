using AbsaCIBdigitalTechAssesment.Functions.API;
using System;
using TechTalk.SpecFlow;

namespace AbsaCIBdigitalTechAssesment
{
    [Binding]
    public class ProduceRandomImageSteps
    {
        APIFunctions api = new APIFunctions();

        [Given(@"I want to perform a request to produce a random image")]
        public void GivenIWantToPerformARequestToProduceARandomImage()
        {
            Console.WriteLine("User wants to do Get Request to Produce random Pictures");
        }

        [When(@"I perform get request to produce a random image")]
        public void WhenIPerformGetRequestToProduceARandomImage()
        {
            api.doGetRandomImageRequest();
        }

        [Then(@"I should see a random image and correct ""([^""]*)"" from api response")]
        public void ThenIShouldSeeARandomImageAndCorrectFromApiResponse(string success)
        {
            api.verifyStatusDescriptionFromResponse(success);
            api.verifyRandomImageFromResponse();
            
        }
    }
}
