using AbsaCIBdigitalTechAssesment.Functions.API;
using System;
using TechTalk.SpecFlow;

namespace AbsaCIBdigitalTechAssesment
{
    
    [Binding]
    public class GetListOfSubBreedsForRetrieverBreedSteps
    {
        APIFunctions api = new APIFunctions();

        [Given(@"I want to perform a request for the list of subBreeds for retriever")]
        public void GivenIWantToPerformARequestForTheListOfSubBreedsForRetriever()
        {
            Console.WriteLine("User wants to do Get request to see the lists of SubBreeds for Retriever Breed...");    
        }

        [When(@"I perform get request  to list of subBreeds for ""([^""]*)""")]
        public void WhenIPerformGetRequestToListOfSubBreedsFor(string retrieverBreed)
        {
            api.doGetRetreiverSubBreedListRequest(retrieverBreed);
        }

 
        [Then(@"I should get correct  subBreeds ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" of Breed and correct ""([^""]*)"" from api response")]
        public void ThenIShouldGetCorrectSubBreedsAndAndAndOfBreedAndCorrectFromApiResponse(string breed1, string breed2, string breed3, string breed4, string statusDescription)
        {
            api.verifyStatusDescriptionFromResponse(statusDescription);
            api.verifyRetrieverSubBreeds(breed1, breed2, breed3, breed4);
        }

    }
}
