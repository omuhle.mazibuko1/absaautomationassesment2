using AbsaCIBdigitalTechAssesment.Functions.API;
using System;
using TechTalk.SpecFlow;

namespace AbsaCIBdigitalTechAssesment
{
    [Binding]
    public class GetAllBreedsSteps
    {
        APIFunctions api = new APIFunctions();

        [Given(@"I want to perform a request that will list all dogs")]
        public void GivenIWantToPerformARequestThatWillListAllDogs()
        {
            Console.WriteLine("User wants to perform Get Request to see All list Of Dogs");
        }

        [When(@"I perform get request to produce a list of all dogs")]
        public void WhenIPerformGetRequestToProduceAListOfAllDogs()
        {
            api.doGetAllDogsRequestToProduceAllBreeds();
           
        }

        [Then(@"I should get a list of dogs succesfully")]
        public void ThenIShouldGetAListOfDogsSuccesfully()
        {
            api.verifyAllDogsReturned();
        }

        [Then(@"I should get a list of dogs and correct ""([^""]*)"" from api response")]
        public void ThenIShouldGetAListOfDogsAndCorrectFromApiResponse(string expectedsStatusDescription)
        {
            api.verifyStatusDescriptionFromResponse(expectedsStatusDescription);
        }

        [Then(@"I should validate if Retreiver is on the list")]
        public void ThenIShouldValidateIfRetreiverIsOnTheList()
        {
            api.verifyRetreiverBreedingFromResponse();
        }



    }
}
