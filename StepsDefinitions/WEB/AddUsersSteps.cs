using AbsaCIBdigitalTechAssesment.Functions.WEB;
using AbsaCIBdigitalTechAssesment.Reusable;
using System;
using TechTalk.SpecFlow;

namespace AbsaCIBdigitalTechAssesment
{
    [Binding]
    public class AddUsersSteps
    {
        WebFunctions web = new WebFunctions();      

        [Given(@"I navigate to way to automation Website")]
        public void GivenINavigateToWayToAutomationWebsite()
        {
            web.NavigateToWaytoAutomate();
            
        }

        [When(@"I land succesfully I should verify if Im on User List table")]
        public void WhenILandSuccesfullyIShouldVerifyIfImOnUserListTable()
        {
            web.isUserListTableSeen();
        }

        [Then(@"I click Add user")]
        public void ThenIClickAddUser()
        {
            web.clickAddUser(); 
        }

        [Then(@"I should enter First User details ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)""")]
        public void ThenIShouldEnterFirstUserDetailsAndAndAndAndAnd(string fname, string lname, string username, string password, string email, string cell)
        {
            web.addFirstUserDetails(fname,lname,username,password,email,cell);
        }

        [Then(@"I should enter Second User details ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)"" and ""([^""]*)""")]
        public void ThenIShouldEnterSecondUserDetailsAndAndAndAndAnd(string fname, string lname, string username, string password, string email, string cell)
        {
            web.addSecondUserDetails(fname, lname, username, password, email, cell);
        }


        [Then(@"I should ensure user is added on the list ""([^""]*)""")]
        public void ThenIShouldEnsureUserIsAddedOnTheList(string fname)
        {
            web.verifyFirstUserWasAddedSuccesful();
        }

        [Then(@"I should ensure second user is added on the list ""([^""]*)""")]
        public void ThenIShouldEnsureSecondUserIsAddedOnTheList(string fname)
        {
            web.verifySecondUserWasAddedSuccesful();
        }

    }
}
