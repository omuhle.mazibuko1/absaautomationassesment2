# absaAutomationAssesment2

# The project itself has packages under packages.config
-You dont have to install them from scratch, but just Right Click on a solution and Restore Nugget Packages and Visual studio will do everything for you/

#Unless the Packages.Config does no have pacakges then The following nugget packages will be required to execute tests:



Install-Package Specflow
Install-Package SpecFlow.NUnit
Install-Package NUnit
Install-Package NUnit.Console
Install-Package Selenium.WebDriver
Install-Package Selenium.Chrome.WebDriver
Install-Package  Microsoft.NET.Test.Sdk
Install-Package selenium.support
Install-Package SpecFlow.Tools.MsBuild.Generation
Install-Package NUnit3TestAdapter
Install-Package RestSharp
Install-Package  ExtentReport.Core
Install-Package Newtonsoft.Json;
Also Install Specflow For Visual Studio under Extensions in visual Studio.

Make sure you install Chrome browser version 85 on your local machine, as the latest version of Selenium.Chrome.WebDriver package is 85, Other than that you can't execute selenium tests
How to clone the project from GitLab:

You can clone this proejct using the link - https://gitlab.com/omuhle.mazibuko1/absaautomationassesment2.git

IDE:
Visual Studio 2022 Community version
C#. Net
Framework Used:
API - RestSharp  used
Web - Selenium used
BDD - Cucumber for .Net(Specflow) used
Unit Test Framework - NUnit
Reporting  - Extent Report
How to Execute Tests:
-Navigate to Test menu on visual Sudio >> Click test Explorer


How to locate Execution Report / Test Evidence:
-Inside the project folder - "AbsaCIBdigitalTechAssesment\bin\index.html" the extent report will be saved there after each test run.
Solution Overview:

This is a Hybrid Framework with Behaviour Driven Development Framework characteristics (features files and Hooks)
Test Data is stored on the feature files.
Configs (web urls,api endpoints) are NOT hard-coded they are stored on the "configs.xml" file.
There is a special class that reads configs.xml files with customized functions to fetch config data.
For selenium tests, The Page Object Model design pattern has been followed which is Object Oriented Programming driven.
Then Build the project >> 5 Tests should appear on Test Explorer as per number of scenarios inside feature files.
Right Click on each test and Click Run.
