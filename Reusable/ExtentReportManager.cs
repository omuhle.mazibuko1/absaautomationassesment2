﻿using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using System.Xml;

namespace AbsaCIBdigitalTechAssesment.Reusable
{
    [Binding]
    public class ExtentReportManager
    {

        static string reportdirectory = Environment.CurrentDirectory;
        static AventStack.ExtentReports.ExtentReports extent;
        static string reportpath = reportdirectory.ToString();
        static AventStack.ExtentReports.ExtentTest feature;
        AventStack.ExtentReports.ExtentTest scenario, step;

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            ExtentHtmlReporter htmlreport = new ExtentHtmlReporter(reportpath);
            extent = new AventStack.ExtentReports.ExtentReports();
            extent.AttachReporter(htmlreport);
        }

        [BeforeFeature]
        public static void BeforeFeature(FeatureContext context1)
        {
            feature = extent.CreateTest(context1.FeatureInfo.Title);
        }

        [BeforeScenario]
        public void BeforeScenarion(ScenarioContext context2)
        {
            scenario = feature.CreateNode(context2.ScenarioInfo.Title);
        }

        [BeforeStep]
        public void BeforeStep()
        {
            step = scenario;
        }

        [AfterStep]
        public void AfterStep(ScenarioContext context3)
        {
            if (context3.TestError == null)
            {
                step.Log(Status.Pass, context3.StepContext.StepInfo.Text);
            }
            else
            {
                step.Log(Status.Fail, context3.StepContext.StepInfo.Text);
            }
        }

        [AfterFeature]
        public static void AfterFeature()
        {
            extent.Flush();
        }
    }

}

