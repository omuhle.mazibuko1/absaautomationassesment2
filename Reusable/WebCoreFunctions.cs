﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using MongoDB.Bson;
using NUnit.Framework.Internal;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AbsaCIBdigitalTechAssesment.Reusable
{
    public class WebCoreFunctions
    {
        public static IWebDriver _driver;

        public static void startWebDriver()
        { 
            _driver = new ChromeDriver();
            _driver.Url = XmlConfigsReaderFunctions.GetWay2AutomationWebUrlLink();
            _driver.Manage().Window.Maximize();
            Console.WriteLine("WebDriver was Started Succesfully...");
        }

        public static IWebElement getElement(By xpath,int timeOutSeconds=20)
        { 
           var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(timeOutSeconds));
           IWebElement element = _driver.FindElement(xpath);
           return element;
        }

        public static int GenerateRandomNumbers()
        {
          int minval = 1; int maxval = 10;
          Random random = new Random();
          return random.Next(minval,maxval);
        }

        public static void verifyTextOfElement(By xpath)
        { 
           IWebElement element = _driver.FindElement(xpath);
            var actualtext = element.Text;

            if (Regex.Match(actualtext, "^[A-Z][a-zA-Z]*$").Success)
            {
                Console.WriteLine("Element Text Found & Matches the String Pattern....");
            }
            else 
            {
                Console.WriteLine("Element Text NOT Retrieved....");
            }
        }

        public static void selectDropDownsByIndex(By xpath,int indexvalue)
        {
            IWebElement selectElement = _driver.FindElement(xpath);
            SelectElement dropDown = new SelectElement(selectElement);
            dropDown.SelectByIndex(indexvalue);
            Console.WriteLine("Drop Down Selected Successfully....");
        }

       
        public static void switchFocusToActiveElement()
        {
            try
            {
                _driver.SwitchTo().ActiveElement();
                Console.WriteLine("Focus on the Active Element has been Set...");
            }
            catch (ElementNotVisibleException ex)
            {
                ex.ToString();
            }
        }

        public static void ImplicitlyWait(int WaitSeconds)
        {
            var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(WaitSeconds));
        }
    }
}
