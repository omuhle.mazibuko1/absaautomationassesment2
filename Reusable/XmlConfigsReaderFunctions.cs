﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AbsaCIBdigitalTechAssesment.Reusable
{
    public class XmlConfigsReaderFunctions
    {
        public static string getXMLConfigPath()
        {
            string fileName = "Configs.xml";
            string currentdirectory = Environment.CurrentDirectory;
            string[] files = System.IO.Directory.GetFileSystemEntries(currentdirectory, fileName);
            string fullDir = currentdirectory + "\\" + fileName;
            if (files[0].Contains(fileName))
            {
                Console.WriteLine("Config was found on:" + fullDir, true);
            }
            else
            {
                Assert.Fail("Config file was not found on the  directory......");
            }

            return fullDir;
        }

        public static string GetWay2AutomationWebUrlLink()
        {
            string mypath = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(getXMLConfigPath());
            XmlElement elt = doc.SelectSingleNode("//WebUrl") as XmlElement;
            if (elt != null)
            {
                mypath = elt.InnerText;
            }
            return mypath;
        }

        public string GetApiBaseUrI()
        {
            string mypath = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(getXMLConfigPath());
            XmlElement elt = doc.SelectSingleNode("//DogApiBaseUrl") as XmlElement;
            if (elt != null)
            {
                mypath = elt.InnerText;
            }
            return mypath;
        }

        public string GetAllDogsBreedsApiEndPoint()
        {
            string mypath = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(getXMLConfigPath());
            XmlElement elt = doc.SelectSingleNode("//GetAllDogsBreedsEndPoint") as XmlElement;
            if (elt != null)
            {
                mypath = elt.InnerText;
            }
            return mypath;
        }

        public string GetRetrieverSubBreedsApiEndPoint()
        {
            string mypath = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(getXMLConfigPath());
            XmlElement elt = doc.SelectSingleNode("//GetRetrieverSubBreedsEndPoint") as XmlElement;
            if (elt != null)
            {
                mypath = elt.InnerText;
            }
            return mypath;
        }

        public string GetRandomImageApiEndPoint()
        {
            string mypath = string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.Load(getXMLConfigPath());
            XmlElement elt = doc.SelectSingleNode("//GetRandomImageEndPoint") as XmlElement;
            if (elt != null)
            {
                mypath = elt.InnerText;
            }
            return mypath;
        }

    }
}
